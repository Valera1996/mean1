var config = require('config.js');
var express = require('express');
var router = express.Router();
var User = require('../controllers/userController');

// USER`s routes
router.get('/', User.getAll);
router.get('/me', User.me);
router.post('/addNewUser', User.add);
router.put('/:_id', User.update);
router.delete('/:_id', User.delete);
router.get('/getAllInfo', User.all);
router.get('/all', User.all);

// AUTH routes

router.post('/register', User.add);
router.post('/login',User.auth);

module.exports = router;





