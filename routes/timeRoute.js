var express = require('express');
var router = express.Router();
var Time = require('../controllers/timeController');

// TIME routes
router.post('/addTime', Time.addTime);
router.get('/getTime', Time.getTime);
router.put('/editTime/:_id', Time.editTime);
router.delete('/time/:_id', Time.deleteTime)

module.exports = router;
