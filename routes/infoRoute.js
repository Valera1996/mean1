var config = require('config.js');
var express = require('express');
var router = express.Router();
var Info = require('../controllers/infoController');

// INFO`s routes
router.get('/getInfo', Info.getInfo);
router.post('/addInfo', Info.addInfo);
router.get('/getInfo', Info.getInfo);
router.put('/editInfo/:_id', Info.editInfo);
router.delete('/info/:_id', Info.deleteInfo);

module.exports = router;



