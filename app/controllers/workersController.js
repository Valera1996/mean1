﻿(function () {
    'use strict';
    angular.module('app').controller('workersController', Controller);

    function Controller(crudService, $scope, $uibModal, userFactory) {
        console.log("workersController loaded");

        initController();

        $scope.logout = function () {
            userFactory.logout();
        };

        function initController() {
            userFactory.getUser().then(function success(response) {
                $scope.worker = response.data;
            });
            crudService.getAllInfo().then(function success(response) {
                $scope.users = response;
            });

            crudService.getAll($scope.currentPage, 3).success(function (response) {
                $scope.itemsPerPage = response.limit;
                $scope.totalItems = response.total;
                $scope.currentPage = response.page;
                $scope.workers = response.docs;
            });


        };

// MODAL WINDOWS

        this.animationsEnabled = true;

        $scope.openAdd = function () {
            var modalInstance = $uibModal.open({
                animation: this.animationsEnabled,
                templateUrl: 'myModalContent.html',
                controller: 'ModalAddCtrl',
                controllerAs: '$addCtrl',
                resolve: {
                    m: function () {
                        return console.log("openAddModal")
                    }
                }
            });
            modalInstance.result.then(function (data) {
                $scope.getall = function() {
                    crudService.getAll($scope.currentPage, $scope.itemsPerPage).success(function (response) {
                        $scope.totalItems = response.total;
                        $scope.currentPage = response.page;
                        $scope.workers = response.docs;
                        $scope.itemsPerPage = response.limit;
                        console.log($scope.workers);
                    });
                };
                setTimeout($scope.getall, 100);
            }, function () {
                $scope.getall = function() {
                    crudService.getAll($scope.currentPage, $scope.itemsPerPage).success(function (response) {
                        $scope.totalItems = response.total;
                        $scope.currentPage = response.page;
                        $scope.workers = response.docs;
                        $scope.itemsPerPage = response.limit;
                        console.log($scope.workers);
                    });
                };
                setTimeout($scope.getall, 100);
            });

        };

        $scope.openEdit = function (worker) {
            var modalInstance = $uibModal.open({
                animation: this.animationsEnabled,
                templateUrl: 'editContent.html',
                controller: 'ModalEditCtrl',
                controllerAs: '$editCtrl',
                resolve: {
                    m: function () {
                        return console.log("openEditModal")
                    },
                    worker: worker
                }
            });
            modalInstance.result.then(function (data) {
                $scope.getall = function() {
                    crudService.getAll($scope.currentPage, $scope.itemsPerPage).success(function (response) {
                        $scope.totalItems = response.total;
                        $scope.currentPage = response.page;
                        $scope.workers = response.docs;
                        $scope.itemsPerPage = response.limit;
                        console.log($scope.workers);
                    });
                };
                setTimeout($scope.getall, 100);
            }, function () {
                $scope.getall = function() {
                    crudService.getAll($scope.currentPage, $scope.itemsPerPage).success(function (response) {
                        $scope.totalItems = response.total;
                        $scope.currentPage = response.page;
                        $scope.workers = response.docs;
                        $scope.itemsPerPage = response.limit;
                        console.log($scope.workers);
                    });
                };
                setTimeout($scope.getall, 100);
            });
        };

        $scope.openInfo = function (worker) {
            var modalInstance = $uibModal.open({
                animation: this.animationsEnabled,
                templateUrl: 'editInfoContent.html',
                controller: 'ModalEditInfo',
                controllerAs: '$infoEdit',
                resolve: {
                    m: function () {
                        return console.log("ModalEditInfo")
                    },
                    worker: worker
                }
            });
            modalInstance.result.then(function (data) {
                $scope.getall = function() {
                    crudService.getAll($scope.currentPage, $scope.itemsPerPage).success(function (response) {
                        $scope.totalItems = response.total;
                        $scope.currentPage = response.page;
                        $scope.workers = response.docs;
                        $scope.itemsPerPage = response.limit;
                        console.log($scope.workers);
                    });
                };
                setTimeout($scope.getall, 100);
            }, function () {
                $scope.getall = function() {
                    crudService.getAll($scope.currentPage, $scope.itemsPerPage).success(function (response) {
                        $scope.totalItems = response.total;
                        $scope.currentPage = response.page;
                        $scope.workers = response.docs;
                        $scope.itemsPerPage = response.limit;
                        console.log($scope.workers);
                    });
                };
                setTimeout($scope.getall, 100);
            });
        };

        $scope.openTime = function (time) {
            var modalInstance = $uibModal.open({
                animation: this.animationsEnabled,
                templateUrl: 'editTime.html',
                controller: 'ModalEditTime',
                controllerAs: '$infoEdit',
                resolve: {
                    m: function () {
                        return console.log("ModalEditTime")
                    },
                    time: time
                }
            });
            modalInstance.result.then(function (data) {

            });
            modalInstance.result.then(function (data) {
                crudService.getAllInfo().then(function success(response) {
                    $scope.users = response;
                });
                $scope.getall = function() {
                    crudService.getAll($scope.currentPage, $scope.itemsPerPage).success(function (response) {
                        $scope.totalItems = response.total;
                        $scope.currentPage = response.page;
                        $scope.workers = response.docs;
                        $scope.itemsPerPage = response.limit;
                        console.log($scope.workers);
                    });
                };
                setTimeout($scope.getall, 100);
            }, function () {
                crudService.getAllInfo().then(function success(response) {
                    $scope.users = response;
                });
                $scope.getall = function() {
                    crudService.getAll($scope.currentPage, $scope.itemsPerPage).success(function (response) {
                        $scope.totalItems = response.total;
                        $scope.currentPage = response.page;
                        $scope.workers = response.docs;
                        $scope.itemsPerPage = response.limit;
                        console.log($scope.workers);
                    });
                };
                setTimeout($scope.getall, 100);
            });
        };
        // CRUD

        $scope.save = function (worker) {
            crudService.Update(worker).then(function () {
            });
        };

        $scope.delete = function (worker) {
            console.log(worker);
            crudService.Delete(worker._id).then(function () {
                crudService.getAll($scope.currentPage, $scope.itemsPerPage).success(function (response) {
                    $scope.totalItems = response.total;
                    $scope.currentPage = response.page;
                    $scope.workers = response.docs;
                    $scope.itemsPerPage = response.limit;
                });
            })
            crudService.deleteInfo(worker.info).then(function () {
            })
            console.log(worker.time);
            crudService.deleteTime(worker.info).then(function () {
            })
        };

        // PAGINATION

        $scope.pageChanged = function () {
            crudService.getAll($scope.currentPage, $scope.itemsPerPage).success(function (response) {
                $scope.totalItems = response.total;
                $scope.currentPage = response.page;
                $scope.workers = response.docs;
                $scope.itemsPerPage = response.limit;
            });
        };

        $scope.setItemsPerPage = function (num) {
            $scope.itemsPerPage = num;
            crudService.getAll($scope.currentPage, $scope.itemsPerPage).success(function (response) {
                $scope.totalItems = response.total;
                $scope.workers = response.docs;
                $scope.itemsPerPage = response.limit;
                $scope.currentPage = 1; //reset to first page  });
            });
        }
    };
})();