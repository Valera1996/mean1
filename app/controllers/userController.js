(function () {
    'use strict';

    angular.module('app').controller('userController', Controller);

    function Controller(crudService, $scope, userFactory) {
        console.log("userController loaded");
        $scope.worker = {};

        initController();

        $scope.logout = function () {
            userFactory.logout();
        };

        function initController() {
            userFactory.getUser().then(function success(response) {
                $scope.user = response.data;
                console.log('user', $scope.user);

                userFactory.getInfo($scope.user.info).then(function success(response) {
                    $scope.worker = response.data;
                    if ($scope.worker) {

                        $scope.save = function (worker) {
                            crudService.editInfo(worker).then(function () {
                            });
                        };

                    }
                    else {

                        $scope.save = function (worker) {
                            worker._id = $scope.user.info;
                            worker.time = $scope.user.info;

                            crudService.addInfo(worker).then(function () {
                                $scope.save = function (worker) {
                                    crudService.editInfo(worker).then(function () { });
                                };
                            });

                        };

                    }
                });
            });
        };
    };
})();
