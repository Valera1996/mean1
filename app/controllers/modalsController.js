var app = angular.module('app');

app.controller('ModalAddCtrl', function ($scope, $uibModalInstance, $http, crudService) {

    function createID() {
        return Math.floor((1 + Math.random()) * 0x10000);
    };
    $scope.addWorker = function () {
        $scope.worker.info = createID();
        $scope.worker.time = $scope.worker.info;
        $scope.worker.populate = $scope.worker.info;
        crudService.addNewUser($scope.worker).then(function () {
        })
    };

    this.ok = function () {
        $uibModalInstance.close('OK');
    }; // submit button

    this.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    }; // cancel button

});

app.controller('ModalEditCtrl', function ($scope, $uibModalInstance, crudService, worker) {

    $scope.worker = worker;

    $scope.saveUser = function () {
        crudService.Update($scope.worker).then(function () {
        })
    };

    this.ok = function () {
        $uibModalInstance.close('OK');
    }; // submit button

    this.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    }; // cancel button

});

app.controller('ModalEditInfo', function ($scope, $uibModalInstance, userFactory, crudService, worker) {

    $scope.workerInfo = worker.info;
console.log('id',$scope.workerInfo);
    userFactory.getInfo($scope.workerInfo).then(function success(response) {
        $scope.info = response.data;
        if ($scope.info) {
            $scope.saveInfo = function () {
                $scope.info._id = $scope.workerInfo;
                $scope.info.time = $scope.workerInfo;
                crudService.editInfo($scope.info).then(function () {
                });
            };
        }
        else {
            $scope.saveInfo = function () {
                $scope.info._id = $scope.workerInfo;
                $scope.info.time = $scope.workerInfo;
                console.log($scope.info);
                crudService.addInfo($scope.info).then(function () {
                });
            };
        }

    });

    $scope.ok = function () {
        $uibModalInstance.close('OK');
    }; // submit button

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    }; // cancel button

});

app.controller('ModalEditTime', function ($scope, $uibModalInstance, userFactory, crudService, time) {

    $scope.timeId = time;
    console.log('id', $scope.timeId);

    userFactory.getTime($scope.timeId).then(function success(response) {
        $scope.time = response.data;
        console.log($scope.time);
        if ($scope.time) {
            $scope.saveTime = function () {
                $scope.time._id = $scope.timeId;
                crudService.editTime($scope.time).then(function () {
                });
            };
        }
        else {
            $scope.saveTime = function () {
                $scope.time._id = $scope.timeId;
                crudService.addTime($scope.time).then(function () {
                });
            };
        }
        ;
    });

    $scope.ok = function () {
        $uibModalInstance.close('OK');
    }; // submit button

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    }; // cancel button

});