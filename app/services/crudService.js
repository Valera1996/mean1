(function () {
    'use strict';

    angular.module('app').factory('crudService', crudService);

    function crudService($http, $q) {

        var service = {};

        service.getAll = getAll;
        service.Create = Create;
        service.Authenticate = Authenticate;
        service.Update = Update;
        service.Delete = Delete;
        service.deleteInfo = deleteInfo;
        service.deleteTime = deleteTime;
        service.addNewUser = addNewUser;
        service.addInfo = addInfo;
        service.editInfo = editInfo;
        service.addTime = addTime;
        service.editTime = editTime;
        service.getAllInfo = getAllInfo;

        return service;

        function getAll(page, limit) {
            return $http.get('/api/users', {
                params: {
                    page: page,
                    limit: limit
                }
            });
        };

        function Create(user) {
            return $http.post('/api/users/register', user).then(handleSuccess, handleError);
        };
        function addNewUser(user) {
            return $http.post('/api/users/addNewUser', user).then(handleSuccess, handleError);
        };
        function addInfo(info) {
            return $http.post('/api/users/addInfo', info).then(handleSuccess, handleError);
        };
        function editInfo(info) {
            return $http.put('/api/users/editInfo/' + info._id, info).then(handleSuccess, handleError);
        };
        function addTime(time) {
            return $http.post('/api/users/addTime', time).then(handleSuccess, handleError);
        };
        function editTime(time) {
            return $http.put('/api/users/editTime/' + time._id, time).then(handleSuccess, handleError);
        };
        function getAllInfo() {
            return $http.get('/api/users/getAllInfo').then(handleSuccess, handleError);
        };

        function Update(user) {
            return $http.put('/api/users/' + user._id, user).then(handleSuccess, handleError);
        };
        function Delete(_id) {
            return $http.delete('/api/users/' + _id).then(handleSuccess, handleError);
        };
        function deleteInfo(_id) {
            return $http.delete('/api/users/info/' + _id).then(handleSuccess, handleError);
        };
        function deleteTime(_id) {
            return $http.delete('/api/users/time/' + _id).then(handleSuccess, handleError);
        };
        function Authenticate(user) {
            return $http.post('/login', user).then(handleSuccess, handleError);
        };
        // private functions
        function handleSuccess(res) {
            return res.data;
        };
        function handleError(res) {
            return $q.reject(res.data);
        };

    };

})();