var mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');
var Schema = mongoose.Schema;

var Users = Schema({
    username: String,
    hash: String,
    role: String,
    populate: Number,
    info: {type: Number, ref: 'Info'},
    time: {type: Number, ref: 'Time'}
});

Users.plugin(mongoosePaginate);
var Users = module.exports = mongoose.model('Users', Users);

