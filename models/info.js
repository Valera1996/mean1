var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Info = Schema({
    _id: {type: Number, ref: 'Users'},
    name: String,
    surname: String,
    patronymic: String,
    post: String,
    gender: String,
    phone: String
});

var Info = module.exports = mongoose.model('Info', Info);




