var Users = require('../models/user'); //user model
var Time = require('../models/time');

// ADD TIME
module.exports.addTime = function (req, res) {
    var time= new Time(req.body);
    time.save(function(err , createdTimeObject) {
        if (err){
            res.send(err);
        }
        res.send(createdTimeObject);
    });

};
// EDIT TIME
module.exports.editTime = function (req, res) {

    var _id=req.body._id;
    var userParam=req.body;

        // fields to update
        var set = {
            startWork: userParam.startWork,
            endWork: userParam.endWork
        };
    Time.findById(_id, function (err, time) {
        // Handle any possible database errors
        if (err) {
            res.status(500).send(err);
        } else {
            time.startWork = set.startWork || time.startWork;
            time.endWork = set.endWork || time.endWork;

            // Save the updated document back to the database
            time.save(function (err, time) {
                if (err) {
                    res.status(500).send(err)
                }
                res.send(time);
            });
        }
    });

};



//  GET TIME FROM USERS
module.exports.getTime = function (req, res) {
    if (!req.user) return res.sendStatus(401);
    var time=req.query.time;
    Users.findOne({time: time}).populate('time').exec(function (err, time) {
        if (err) return handleError(err);
        res.send(time.time);
    });
};
// // DELETE TIME
module.exports.deleteTime = function (req, res) {
    var id=req.params._id;

    Time.findOne({ _id: id}, function (err, time) {
        if(time!==null)
            time.remove(function (err) {});
        res.send(time);
    });

};