var config = require('config.js');
var Users = require('../models/user'); //user model
var Info = require('../models/info'); //info model

// ADD INFO
module.exports.addInfo = function (req, res) {
    var worker = new Info(req.body);
    worker.save(function (err, createdTodoObject) {
        if (err) {
            res.send(err);
        }
        res.send(createdTodoObject);
    });
};
// EDIT INFO
module.exports.editInfo = function (req, res) {

    var _id=req.body._id;
    var userParam=req.body;
        // fields to update
        var set = {
            name: userParam.name,
            surname: userParam.surname,
            patronymic: userParam.patronymic,
            post: userParam.post,
            gender: userParam.gender,
            phone: userParam.phone
        };
    Info.findById(_id, function (err, info) {
        // Handle any possible database errors
        if (err) {
            res.status(500).send(err);
        } else {

            info.name = set.name || info.name;
            info.surname = set.surname || info.surname;
            info.patronymic = set.patronymic || info.patronymic;
            info.post = set.post || info.post;
            info.gender = set.gender || info.gender;
            info.phone = set.phone || info.phone;

            // Save the updated document back to the database
            info.save(function (err, info) {
                if (err) {
                    res.status(500).send(err)
                }
                res.send(info);
            });
        }
    });

};
//  GET INFO FROM USERS
module.exports.getInfo = function (req, res) {
    if (!req.user) return res.sendStatus(401);
    var info=req.query.info;
    Users.findOne({info: info}).populate('info').exec(function (err, info) {
        if (err) return handleError(err);
        res.send(info.info);
    });
};
// // DELETE INFO
module.exports.deleteInfo = function (req, res) {
    var id=req.params._id;
    Info.findOne({ _id: id}, function (err, info) {
        if(info!==null)
            info.remove(function (err) {});
        res.send(info);
    });

};
