var config = require('config.js');
var _ = require('lodash');
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var Q = require('q');
var Users = require('../models/user');

// // GET AUTH USER
module.exports.me = function (req, res) {
    res.send(req.user);
};
// // GET ALL USERS
module.exports.getAll=function (req, res) {
    // if (!req.user) return res.sendStatus(401);
    var page = +req.query['page'];
    var limit = +req.query['limit'];
    Users.paginate({}, {page: page, limit: limit}, function (err, data) {
        res.send(data);
    });
};

// // ADD NEW USER
module.exports.add = function (req) {
    var userParam=req.body;
    var deferred = Q.defer();
    // VALIDATION
    Users.findOne(
        {username: userParam.username},
        function (err, user) {
            if (err)
                deferred.reject(err);
            if (user) {
                // username already exists
                deferred.reject('Username "' + userParam.username + '" is already taken');
            } else {
                createUser();
            }
        });
    function createUser() {
        // set user object to userParam without the cleartext password
        var user = _.omit(userParam, 'password');
        // add hashed password to user object
        user.hash = bcrypt.hashSync(userParam.password, 10);
        Users.create(
            user,
            function (err, doc) {
                if (err) deferred.reject(err);

                deferred.resolve();
            });
    };
    return deferred.promise;
};
// UPDATE NEW USER
module.exports.update = function (req, res) {

    var _id=req.body._id;
    var userParam=req.body;
        var set = {
            username: userParam.username,
            role: userParam.role
        };
        // update password if it was entered
        if (userParam.password) {
            set.hash = bcrypt.hashSync(userParam.password, 10);
        }
    Users.findById(_id, function (err, user) {
        // Handle any possible database errors
        if (err) {
            res.status(500).send(err);
        } else {

            user.username = set.username || user.username;
            user.role = set.role || user.role;
            user.hash = set.hash || user.hash;

            // Save the updated document back to the database
            user.save(function (err, user) {
                if (err) {
                    res.status(500).send(err)
                }
                res.send(user);
            });
        }
    });
};
// // DELETE USER
module.exports.delete = function (req, res) {
    var id=req.params._id;
    Users.findOne({ _id: id}, function (err, user) {
        if(user!==null)
            user.remove(function (err) {});
        res.send(user);
    });

};

module.exports.deleteUser = function (_id, callback) {
    var query = {_id: _id};
    Users.remove(query, callback);
};

// AUTH NEW USER
module.exports.auth = function (req, res) {
    var body = req.body;
    auth (body.username, body.password)
        .then(function (token) {
            if (token) {
                // authentication successful
                res.send({token: token, user: body});
            } else {
                // authentication failed
                res.status(401).send('Username or password is incorrect');
            }
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
};
// // GET ALL USERS WITH ROLE 'Пользователь'
module.exports.all = function (info, res) {
    Users.find({role:'Пользователь'}).populate('info').populate('time').exec(function (err, info) {
        if (err) return handleError(err);
        res.send(info);
    });
};
// auth function
function auth (username, password) {
    // console.log(username,password);
    var deferred = Q.defer();
    Users.findOne({username: username}, function (err, user) {
        if (err) deferred.reject(err);
        if (user && bcrypt.compareSync(password, user.hash)) {
            // authentication successful
            deferred.resolve(jwt.sign({
                _id: user._id,
                username: user.username,
                role: user.role,
                info: user.info
            }, config.secret));
        } else {
            // authentication failed
            deferred.resolve();
        }
    });
    return deferred.promise;
};

